<?php
require_once "header.php";
?>

<main class="col-md-9">
    <h1>Обход бинарного дерева</h1>
    <p><b>Описание задачи:</b>Создайте функцию обхода бинарного дерева summTree,
        которая вернет сумму значений, лежащих в узлах. Все значения в узлах -
        целые числа, дерево может быть произвольного размера. </p>
    <div class="task">
        <form action="" method="post">
            <button type="submit" class="btn btn-primary">Найти сумму бинарного
                дереве
            </button>
        </form>
        <?

        //бинарное дерево
        $tree = [
            "value" => 12,
            "right" => [
                "value" => -5
            ],
            "left"  => [
                "value" => -61,
                "right" => [
                    "value" => 35,
                    "left"  => [
                        "value" => 3
                    ],
                    "right" => [
                        "value" => -18
                    ]
                ],
                "left"  => [
                    "value" => 45
                ]
            ]
        ];
        echo "<pre>";
        print_r($tree);
        echo "</pre>";

        //вызов функции
        sumTree($tree);

        //пользовательская функция, которая находит сумму значений, лежащих в узлах
        function sumTree($array)
        {

            foreach ($array as $item) {
                echo "<br>$item";
                $key = key($array);
                echo "<br>ключ - $key";
                if ($key == "right") {
                    echo "<br>правая вершина";
                    return sumTree(next($array));
                } elseif ($key == "left") {
                    echo "<br>левая вершина";
                    return sumTree(next($array));
                }
                next($array);

            }

        }


        ?>
    </div>
</main>


<?php
require_once "footer.php";
?>

