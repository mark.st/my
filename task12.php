<?php
require_once "header.php";
?>

<main class="col-md-9">
    <h1>Нахождение пиков</h1>
    <p><b>Описание задачи:</b>Написать функцию pickPeaks, принимающую массив
        целых положительных чисел от 1 и более, возвращающую ассоциативный
        массив с двумя ключами: «pos» и «peaks». Оба они должны быть
        неассоциативными массивами. Если в данном массиве нет пика, просто
        верните ['pos' => [], 'peaks' => []]. </p>
    <div class="task">
        <form action="" method="post">
            <button type="submit" class="btn btn-primary">Найти пики</button>
        </form>
        <?


        //массивы
        $arr  = [3, 2, 3, 6, 4, 1, 2, 3, 2, 1, 2, 3];
        $arr2 = [1, 2, 2, 2, 1];
        $arr3 = [1, 2, 2, 2, 3];
        $arr4 = [1, 2, 2, 2, 2];

        //вызов функции
        pickPeaks($arr);
        echo "<hr>";

        pickPeaks($arr2);
        echo "<hr>";

        pickPeaks($arr3);
        echo "<hr>";

        pickPeaks($arr4);

        //пользовательская функция, которая находит пики
        function pickPeaks($array)
        {
            //результирующий массив с пиками и их позициями
            $result = ['pos' => [], 'peaks' => []];

            for ($i = 1; $i < count($array) - 1; $i++) {

                //условие для простого пика
                if ($array[$i] > $array[$i + 1]
                    && $array[$i] > $array[$i - 1]
                ) {

                    //добавление позиции и пика
                    array_push($result['peaks'], $array[$i]);
                    array_push($result['pos'], $i);

                  //начало плато
                } elseif ($array[$i] > $array[$i - 1]
                          && $array[$i] == $array[$i + 1]
                ) {

                    //фиксация начало плато
                    $plato = $array[$i];
                    $index_plato = $i;

                    //проходим всё плато
                    while ($array[$i] == $plato) {
                        $i++;
                    }
                    //если элемент, который находится после плато больше, то это пик
                    if ($array[$i - 1] > $array[$i] && $array[$i] != "") {
                        //добавление позиции и индекс начала плато
                        array_push($result['peaks'], $plato);
                        array_push($result['pos'], $index_plato);
                    }
                }

            }

            //вывод массива
            foreach ($result as $key) {
                $key_name = key($result);
                echo "'$key_name'=>[";
                foreach ($key as $item) {
                    echo "$item, ";
                }
                echo "], ";
                next($result);
            }
        }

        ?>
    </div>
</main>


<?php
require_once "footer.php";
?>

