<?php
require_once "header.php";
?>

<main class="col-md-9">
    <h1>Хранение данных клиентов</h1>
    <p><b>Описание задачи: </b>Создайте функцию, принимающую массив-хранилище
        данных, произвольный массив с новыми данными и имя клиента, позволяющую
        дописать произвольные данные клиента в массив-хранилище. Функция должна
        возвращать массив, в который были добавлены данные в случае, если клиент
        существует, либо false в случае, если клиент не найден в
        массиве-хранилище.</p>

    <div class="task">
        <form action="" method="post">
            <label>Имя</label>
            <input name="name" type="name" class="form-control"
                   placeholder="Введите имя пользователя">
            <label>Возраст</label>
            <input name="age" type="name" class="form-control"
                   placeholder="Введите имя пользователя">
            <label>Зарплата</label>
            <input name="money" type="name" class="form-control"
                   placeholder="Введите имя пользователя">
            <label>Телефон</label>
            <input name="tel" type="tel" class="form-control"
                   placeholder="Введите имя пользователя">
            <label>Машина</label>
            <input name="car" type="name" class="form-control"
                   placeholder="Введите имя пользователя">
            <br>

            <button type="submit" class="btn btn-primary">Добавить</button>
        </form>
        <?
        $name = $_POST['name'];

        //массив с пользователями
        $clientsDataArray = [
            'Tom'  => [
                'Age'   => 21,
                'Money' => '350$'
            ],
            'Mike' => [
                'Age'   => 38,
                'Car'   => 'Toyota',
                'Phone' => 8432848238
            ],
            'John' => [
                'Car' => 'Mercedes'
            ]
        ];

        //массив с ведёнными данными
        $newClientDataArray = [
            'Age'   => $_POST['age'],
            'money' => $_POST['money'],
            'Phone'   => $_POST['tel'],
            'Car'   => $_POST['car']
        ];


        //вызов функции
        AddUser($clientsDataArray, $newClientDataArray, $name);

        //пользовательская функция, которая добавляет данные пользователя
        function AddUser($clientsDataArray, $newClientDataArray, $name)
        {
            //имя клиента из формы
            $name = $_POST['name'];

            //проверка на существование клиента в исходном массиве
            if (array_key_exists($name, $clientsDataArray)) {
                //добавление массива с данными к клиенту
                $clientsDataArray[$name] += $newClientDataArray;

                //вывод итогового массива
                foreach ($clientsDataArray as $name) {
                    echo "<ul>".key($clientsDataArray);
                    //перебор массива с данными пользователя
                    foreach ($name as $item) {
                        $key = key($name);
                        if($item != "")
                        echo "<ul>$key: " . $item."</ul>";
                        next($name);
                    }
                    next($clientsDataArray);
                    echo "</ul>";
                }
            } else {
                echo "<b>Такого пользователя нет</b>";
            }
        }

        ?>
    </div>
</main>


<?php
require_once "footer.php";
?>

